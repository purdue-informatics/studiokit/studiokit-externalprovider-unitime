﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.UniTime.Models;

public class UniTimeGroup : ModelBase, IOwnable
{
	[Required]
	public int ExternalProviderId { get; set; }

	[ForeignKey(nameof(ExternalProviderId))]
	public UniTimeExternalProvider ExternalProvider { get; set; }

	[Required]
	public int ExternalTermId { get; set; }

	[ForeignKey(nameof(ExternalTermId))]
	public IExternalTerm ExternalTerm { get; set; }

	[Required]
	public string CreatedById { get; set; }

	[ForeignKey(nameof(CreatedById))]
	public IUser User { get; set; }

	[Required]
	public string ExternalId { get; set; }

	[Required]
	public string Name { get; set; }

	public string Description { get; set; }
}