﻿using System.ComponentModel.DataAnnotations;

namespace StudioKit.ExternalProvider.UniTime.Models;

public class UniTimeExternalProvider : ExternalProvider.Models.ExternalProvider
{
	[Required]
	public string Key { get; set; }

	[Required]
	public string Secret { get; set; }
}