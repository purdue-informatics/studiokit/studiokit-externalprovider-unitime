﻿using System;

namespace StudioKit.ExternalProvider.UniTime.Exceptions;

public class UniTimeException : Exception
{
	public UniTimeException()
	{
	}

	public UniTimeException(string message)
		: base(message)
	{
	}

	public UniTimeException(string message, Exception inner)
		: base(message, inner)
	{
	}
}