﻿namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Models;

public class Course
{
	public string SubjectArea { get; set; }
	public string CourseNumber { get; set; }
	public string CourseTitle { get; set; }
	public string ClassSuffix { get; set; }
}