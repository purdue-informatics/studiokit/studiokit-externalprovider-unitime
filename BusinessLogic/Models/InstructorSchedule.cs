﻿using System.Collections.Generic;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Models;

public class InstructorSchedule
{
	public IEnumerable<Class> Classes { get; set; }
}