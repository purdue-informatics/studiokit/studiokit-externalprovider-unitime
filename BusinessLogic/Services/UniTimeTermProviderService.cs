﻿using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Models;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities;
using StudioKit.ExternalProvider.UniTime.Exceptions;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.ExternalProvider.UniTime.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Services;

public class UniTimeTermProviderService<TExternalTerm> : ITermProviderService<TExternalTerm>
	where TExternalTerm : class, IExternalTerm, new()
{
	private readonly UniTimeApiService _uniTimeApiService;

	public UniTimeTermProviderService(UniTimeApiService uniTimeApiService)
	{
		_uniTimeApiService = uniTimeApiService ?? throw new ArgumentNullException(nameof(uniTimeApiService));
	}

	public async Task<IEnumerable<TExternalTerm>> GetTermsAsync(ExternalProvider.Models.ExternalProvider externalProvider,
		CancellationToken cancellationToken = default)
	{
		if (!(externalProvider is UniTimeExternalProvider uniTimeProvider))
			throw new ArgumentException(Strings.UniTimeExternalProviderRequired);

		try
		{
			var roles = await _uniTimeApiService.GetResultAsync<List<Role>>(
				UniTimeApiService.GetCredentials(uniTimeProvider),
				UniTimeEndpoints.GetRolesUrl(),
				cancellationToken);
			var eventZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
			return roles
				.Where(r => r.Campus == "PWL") // TODO - TermSync: determine how to handle multiple campuses
				.Select(role => new TExternalTerm
				{
					ExternalProviderId = uniTimeProvider.Id,
					ExternalId = $"{role.Term}{role.Year}{role.Campus}",
					Name = $"{role.Term} {role.Year}",
					StartDate = TimeZoneInfo.ConvertTimeToUtc(role.EventBeginDate, eventZone),
					EndDate = TimeZoneInfo.ConvertTimeToUtc(role.EventEndDate, eventZone),
				});
		}
		catch (Exception e)
		{
			throw new UniTimeException(Strings.GetTermsFailed, e);
		}
	}
}