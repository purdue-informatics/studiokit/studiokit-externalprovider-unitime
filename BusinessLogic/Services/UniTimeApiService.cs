﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StudioKit.Encryption;
using StudioKit.ExternalProvider.UniTime.Exceptions;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.TransientFaultHandling.Http.Constants;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Services;

public class UniTimeApiService
{
	private readonly ILogger<UniTimeApiService> _logger;
	private readonly Func<IHttpClient> _retryingHttpClientFactory;

	public UniTimeApiService(
		ILogger<UniTimeApiService> logger,
		Func<IHttpClient> retryingHttpClientFactory)
	{
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		_retryingHttpClientFactory = retryingHttpClientFactory ?? throw new ArgumentNullException(nameof(retryingHttpClientFactory));
	}

	public static string GetCredentials(UniTimeExternalProvider externalProvider)
	{
		var decryptedUsername = EncryptedConfigurationManager.TryDecryptSettingValue(externalProvider.Key);
		var decryptedPassword = EncryptedConfigurationManager.TryDecryptSettingValue(externalProvider.Secret);
		return Base64Encode($"{decryptedUsername}:{decryptedPassword}");
	}

	public static string Base64Encode(string plainText)
	{
		var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
		return Convert.ToBase64String(plainTextBytes);
	}

	public static string Base64Decode(string base64EncodedData)
	{
		var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
		return Encoding.UTF8.GetString(base64EncodedBytes);
	}

	#region HttpClient

	public async Task<T> GetResultAsync<T>(string credentials, string url, CancellationToken cancellationToken)
	{
		var json = await GetApiResultAsync(credentials, url, cancellationToken);
		return DeserializeResult<T>(json);
	}

	public async Task<string> GetApiResultAsync(string credentials, string url, CancellationToken cancellationToken)
	{
		string json;
		var builder = new Uri(url);

		_logger.LogDebug("Getting API result with url {0}", url);
		var client = _retryingHttpClientFactory();
		try
		{
			json = await client
				.GetStringAsync(builder, cancellationToken, new AuthenticationHeaderValue(AuthorizationHeaderType.Basic, credentials))
				.ConfigureAwait(false);
		}
		catch (HttpRequestException ex)
		{
			if (ex.Message.Contains("400"))
				return "";
			throw new UniTimeDownException("UniTime API down", ex);
		}

		_logger.LogDebug("Results fetched from API for {0}", builder.AbsolutePath);
		_logger.LogDebug("Result: {0}", json);
		return json;
	}

	private T DeserializeResult<T>(string json)
	{
		_logger.LogDebug("Deserializing json: {json}", json);
		try
		{
			return JsonConvert.DeserializeObject<T>(json);
		}
		catch (JsonReaderException e)
		{
			throw new UniTimeInvalidJsonException($"Invalid UniTime JSON: {json}", e);
		}
	}

	#endregion HttpClient
}