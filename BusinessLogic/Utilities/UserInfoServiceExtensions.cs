﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.UserInfoService;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities;

public static class UserInfoServiceExtensions
{
	public static (List<TUser>, List<string>) GetUserInfoUsersByIdentifiers<TUser>(
		this IUserInfoService userInfoService,
		IUserFactory<TUser> userFactory,
		List<string> identifiers)
		where TUser : IUser, new()
	{
		var invalidIdentifiers = new List<string>();

		// Alias: length is from 2 to 8 characters
		var aliasIdentifiers = identifiers
			.Where(identifier => !identifier.Contains('@') && !identifier.All(char.IsDigit) && identifier.Length is >= 2 and <= 8)
			.Distinct()
			.ToList();

		// PUID: length can be only 8 digits, or 10 (with the two leading zeros)
		var puidIdentifiers = identifiers
			.Where(identifier => identifier.All(char.IsDigit) && (identifier.Length == 8 || identifier.Length == 10))
			.Distinct()
			.ToList();

		// Email: find identifiers that are emails, attempt to extract aliases
		var emailIdentifiers = identifiers
			.Where(identifier => identifier.Contains('@'))
			.ToList();
		if (emailIdentifiers.Any())
		{
			aliasIdentifiers = aliasIdentifiers.Concat(
					emailIdentifiers.Select(identifier =>
					{
						var parts = identifier.Split('@');
						var alias = parts.Length == 2 ? parts[0] : null;
						// if substring is not an alias, track the original identifier as invalid
						if (alias == null || alias.All(char.IsDigit) || alias.Length < 2 || alias.Length > 8)
							invalidIdentifiers.Add(identifier);
						return alias;
					}))
				.Distinct()
				.ToList();
		}

		// Track invalid identifiers
		invalidIdentifiers.AddRange(
			identifiers
				.Except(emailIdentifiers) // we already tracked invalid email identifiers
				.Where(identifier => !aliasIdentifiers.Contains(identifier) && !puidIdentifiers.Contains(identifier))
		);

		// Get the user info for each set of identifiers, if any
		var aliasUserInfoList = aliasIdentifiers.Any()
			? userInfoService.GetUserList(aliasIdentifiers.ToArray())
			: new List<UserInfo>();
		var puidUserInfoList = puidIdentifiers.Any()
			? userInfoService.GetUserList(puidIdentifiers.ToArray())
			: new List<UserInfo>();

		// Track invalid identifiers that did not return user info
		invalidIdentifiers.AddRange(aliasIdentifiers
			.Where(alias => aliasUserInfoList.All(userInfo => !alias.Equals(userInfo.CareerAccountAlias))));
		invalidIdentifiers.AddRange(puidIdentifiers
			.Where(puid => puidUserInfoList.All(userInfo => !puid.Equals(userInfo.Puid))));

		// Combine results
		var users = aliasUserInfoList
			.Concat(puidUserInfoList)
			// Distinct by PUID
			.GroupBy(u => u.Puid)
			.Select(g => g.First())
			// Create user objects
			.Select(u => userFactory.CreateUser(
				u.Email,
				u.Email,
				u.FirstName,
				u.LastName,
				u.Puid,
				u.CareerAccountAlias))
			.ToList();

		return (users, invalidIdentifiers);
	}
}